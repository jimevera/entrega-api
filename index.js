// Utilidad - variables de entorno (archivo .env)
require("dotenv").config();

// Dependencias externas
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const corsOptions = {
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "allowedHeaders": "*",
  "exposedHeaders": "*",
  "credentials": true,
  "preflightContinue": false,
  "optionsSuccessStatus": 204
};



// Middlewares
const checkIfTheUserHasCredentials = require("./middlewares/checkIfTheUserHasCredentials");

// Controllers
const login = require("./controllers/users/login");
const register = require("./controllers/users/register");
const oponentes = require("./controllers/users/oponentes");

const listGame = require("./controllers/games/listGame");
const createGame = require("./controllers/games/createGame");

const listUsers = require("./controllers/ranking/listUsers");
const initGame = require("./controllers/games/initGame");

const choice = require("./controllers/games/choice");


// Conectarse a la base de datos
let databaseConnectionString;
if (process.env.DB_USER && process.env.DB_PASSWORD) {
  databaseConnectionString = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
} else {
  databaseConnectionString = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;
}

// Iniciar express
const app = express();

// Middleware para parseo de JSON (application/json)
app.use(bodyParser.json());

// Middleware para manejo de settings CORS
app.use(cors());
app.use(cors(corsOptions));



// Validacion de credenciales
app.post('/login', login);

// Creacion de usuario nuevo
app.post('/register', register);


// Listado de oponentes
app.get('/oponentes', checkIfTheUserHasCredentials, oponentes);


// Creacion de juego nuevo
app.post("/games", checkIfTheUserHasCredentials, createGame);

// Obtener una partida dado su ID
app.get('/games/:id', checkIfTheUserHasCredentials, initGame);

// Listar todos las partidas
app.get('/games', checkIfTheUserHasCredentials, listGame);

app.put('/games/choice/:id/:choice', checkIfTheUserHasCredentials, choice);


// Listar las partidas finalizadas
//app.get('/games', checkIfTheUserHasCredentials, listGameFinish)

//Listar usuarios Ranking
app.get('/ranking', checkIfTheUserHasCredentials, listUsers);


// Conexion a la base de datos
mongoose.connect(
  databaseConnectionString,
  { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true },
  (error) => {
    if (error) {
      console.error("No fue posible conectarse a la base de datos", error);
    } else {
      // Comenzar a escuchar por conexiones
      app.listen(process.env.PORT, () =>
        console.log(`;) Servidor corriendo en el puerto: ${process.env.PORT}`)
      );
    }
  }
);
