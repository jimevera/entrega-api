const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const userModel = require('../../models/user')

module.exports = (request, response) => {
    userModel.findOne({ 'email': request.body.email }, (error, user) => {
        if (error) {
            response.status(500).json({
                message: 'Error al intentar iniciar sesion'
            })
        } else {
            if (user) {
                const match = bcrypt.compareSync(request.body.password, user.password)

                if (match) {
                    const userWithoutPassword = user.toObject()

                    delete userWithoutPassword.password

                    // token de usuario
                    userWithoutPassword.token = jwt.sign({
                        id: user._id,

                    }, process.env.JWT_KEY, { expiresIn: '7d' })

                    response.json({
                        user: userWithoutPassword
                    })
                } else {
                    response.status(401).end()
                }
            } else {
                response.status(401).end()
            }
        }
    })
}
