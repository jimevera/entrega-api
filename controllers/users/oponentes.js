const userModel = require('../../models/user')

module.exports = (request, response) => {
    userModel.find({ _id: {$ne:request.user.id}}, (error, oponentes) => {
        if (error) {
            response.status(500).json({
                message: 'No se pudieron obtener los oponentes'
            })
        } else {

            response.json({
                oponentes
            })
        }
    })
}

