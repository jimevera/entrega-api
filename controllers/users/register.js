const bcrypt = require('bcrypt')
const Joi = require('@hapi/joi')
const jwt = require('jsonwebtoken')
const userModel = require('../../models/user')

module.exports = (request, response) => {
    const schema = Joi.object({
        password: Joi.string()
            .alphanum()
            .min(7)
            .max(50)
            .required(),

        email: Joi.string()
            .email()
            .required()
    })

    const validationResult = schema.validate(request.body)

    if (!validationResult.error) {
        const passwordHash = bcrypt.hashSync(request.body.password, 2)

        userModel.create({
            password: passwordHash,
            email: request.body.email,
        }, (error, user) => {
            if (error) {
                response.status(500).json({
                    message: 'No se pudo registrar el usuario'
                })
            } else {
                const userWithoutPassword = user.toObject()

                delete userWithoutPassword.password

                userWithoutPassword.token = jwt.sign({
                    id: user._id,

                }, process.env.JWT_KEY, { expiresIn: '7d' })

                response.json({
                   user: userWithoutPassword
                })
            }
        })
    } else {
        response.status(400).json({
            message: validationResult.error
        })
    }
}
