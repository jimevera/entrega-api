const userModel = require('../../models/user')

module.exports = (request, response) => {
    userModel.find((error, users) => {
        if (error) {
            response.status(500).json({
                message: 'No se pudieron obtener los usuarios'
            })
        } else {
            response.json({
                users
            })
        }
    })
}