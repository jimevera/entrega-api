const gameModel = require('../../models/game')

module.exports = (request, response) => {
    gameModel.findById(request.params.id, (error, game) => {
        if (error) {
            response.status(500).json({
                message: 'No se encontro el libro'
            })
        } else {
            // Retornamos la partida
            response.json({
                game
            })
        }
    })
}