const gameModel = require('../../models/game')

module.exports = (request, response) => {
    gameModel.find({ $or: [ { id: request.user.id }, { idOponente: request.user.id }]}, (error, games) => {       
        if (error) {
            response.status(500).json({
                message: 'No se pudieron obtener las jugadas'
            })
        } else {
            response.json({
                games
            })
        }
    })
}