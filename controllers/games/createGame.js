const Joi = require("@hapi/joi");
const gameModel = require("../../models/game");

module.exports = (request, response) => {
  const schema = Joi.object({
    idOponente: Joi.string()
        .required(), 
  });

  const validationResult = schema.validate(request.body);

  if (!validationResult.error) {
    gameModel.create(
      {
        id: request.user.id,
        idOponente: request.body.idOponente,
      },

      (error, game) => {
        if (error) {
          console.error(error)
          response.status(500).json({
            message: "No se pudo crear la partida",
          });
        } else {
          response.json({
            game,
          });
        }
      }
    );
  } else {
    response.status(400).json({
      message: validationResult.error,
    });
  }
};
