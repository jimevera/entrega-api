const gameModel = require('../../models/game');

module.exports = (request, response) => {
    gameModel.findOne({ $and: [{ $or: [ { id: request.user.id }, { idOponente: request.user.id }]}, {_id: request.params.id}]}, (error, game) => {       
        
        const choice = parseInt(request.params.choice);
        

        if (error) {
            response.status(500).json({
                message: 'No se pudieron obtener las partidas'
            })
        } else {
            console.log(game)
            if (!game.enCurso) {
                response.status(400).json({
                    message: 'La partida ya ha finalizado'
                })
            }
            //usuario que comenzo la partida
            if (request.user.id === game.id) {
                //me fijo si es divisible por 2, el turno de id
                if (game.choices.length %2 === 0) {
                    if (choice === 1 || choice === 2 || choice === 3) {
                        game.choices.push(request.params.choice)
                        game.save(error => {
                            if (error) {
                                response.status(500).json({
                                    message: 'No se pudo guardar la jugada'
                                })
                            }
                        })
                        response.status(200).json({
                            message: 'Jugada guardada'
                        })
                    } else (
                        response.status(400).json({
                            message: 'Jugada invalida'
                        })
                    )
                }
                else {
                    response.status(400).json({
                        message: 'No es su turno'
                    })
                }      
            } else {
                //usuario no comenzo la partida
                //me fijo si es el turno del oponente
                if (!game.choices.length %2 === 0) {
                    if (choice === 1 || choice === 2 || choice === 3) {
                        
                        
                        //me fijo quien gana la mano
                        const [puntosId, puntosIdOponente] = obtenerGanador(game.choices[game.choices.length -1], choice);
                        game.puntosId = game.puntosId + puntosId;
                        game.puntosIdOponente = game.puntosIdOponente + puntosIdOponente;
                        
                        game.choices.push(request.params.choice)
                        
                        console.log(game.choices[game.choices.length -1])

                       
                        if ((game.choices.length) /2 === 3) {
                            game.enCurso = false
                        }



                        game.save(error => {
                            if (error) {
                                response.status(500).json({
                                    message: 'No se pudo guardar la jugada'
                                })
                            }
                        }) 
                        response.status(200).json({
                            message: 'Jugada guardada'
                        })
                    } else (
                        response.status(400).json({
                            message: 'Jugada invalida'
                        })
                    )

                } else {
                    response.status(400).json({
                        message: 'No es su turno'
                    })
                }     
            }
        }
    });
};

const obtenerGanador = (manoId, manoIdOponente) => {
    switch(manoId) {
        case 1: 
            if (manoIdOponente === 1) {
                return [0,0]
            } 
            if (manoIdOponente === 2) {
                return [0,1]
            }
            return [1,0]
        case 2:
            if (manoIdOponente === 1) {
                return [1,0]
            } 
            if (manoIdOponente === 2) {
                return [0,0]
            }
            return [0,1]
        case 3:
            if (manoIdOponente === 1) {
                return [0,1]
            } 
            if (manoIdOponente === 2) {
                return [1,0]
            }
            return [0,0]
    }
};