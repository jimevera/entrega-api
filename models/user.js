const { model, Schema } = require('mongoose')

module.exports = model('users', new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
}))