const { model, Schema } = require('mongoose')

module.exports = model('games', new Schema({
    id: {
        type: String,
        required: true,
        trim: true 
    },  

    idOponente: {
        type: String,
        required: true,
        trim: true 
    },

    enCurso: {
        type: Boolean,
        required: true,
        default: true
    },

    choices: [
        {
            type: Number,
        }
    ],

    puntosId: {
        type: Number,
        required: true,
        default: 0
    },

    puntosIdOponente: {
        type: Number,
        required: true,
        default: 0
    }
}));
