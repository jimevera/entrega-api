// Utilidad - variables de entorno (archivo .env)
require('dotenv').config()

// Dependencias externas
const bcrypt = require('bcrypt')
const faker = require('faker')
const mongoose = require('mongoose')

// Modelos
const userModel = require('./models/user')

// Conectarse a la base de datos
let databaseConnectionString;
if (process.env.DB_USER && process.env.DB_PASSWORD) {
    databaseConnectionString = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_NAME}?retryWrites=true&w=majority`
} else {
    databaseConnectionString = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`
}

// Documentos a insertar en las colecciones
const users = []


// Password usuarios
const userPassword = bcrypt.hashSync('contra123', 2)

// Generar una lista de 10 usuarios
for (let numeroDeIteracion = 0; numeroDeIteracion < 10; numeroDeIteracion++) {
    users.push({
        email: faker.internet.email(),
        password: userPassword
    })
}

// Mostramos datos utiles en la consola
console.log('#############################')
console.log('Seed de datos')
console.log('#############################')
console.log('Se van a insertar:')
console.log(`${users.length} Usuarios`)


// Conexion a la base de datos
mongoose.connect(databaseConnectionString, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, (error) => {
    if (error) {
        console.error('No fue posible conectarse a la base de datos', error)
    } else {
        Promise.all([
            userModel.insertMany(users)
        ]).then(docs => {
            mongoose.connection.close()
        })
    }
})
